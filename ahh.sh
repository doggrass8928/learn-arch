export LC_ALL=C

upnpc="upnpc -u http://192.168.1.1:80/DeviceDescription.xml"
external=3333
port=22
ip=$($upnpc -l | grep "Local LAN ip address" | cut -d: -f2)

$upnpc -d $external TCP >/dev/null 2>&1
$upnpc -a $ip $port $external TCP >/dev/null 2>&1

